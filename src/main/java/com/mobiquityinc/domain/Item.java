package com.mobiquityinc.domain;

import java.math.BigDecimal;

/**
 * The Objects of this class will keep each item of each line in the file.
 * 
 * @author Arvind
 *
 */
public class Item {

	/**
	 * Index number of the item.
	 */
	private int indexNumber;
	/**
	 * Weight of the item
	 */
	private BigDecimal weight;
	/**
	 * cost of the item
	 */
	private BigDecimal cost;

	/**
	 * @return the weight
	 */
	public BigDecimal getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	/**
	 * @return the cost
	 */
	public BigDecimal getCost() {
		return cost;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	/**
	 * @return the indexNumber
	 */
	public int getIndexNumber() {
		return indexNumber;
	}

	/**
	 * @param indexNumber
	 *            the indexNumber to set
	 */
	public void setIndexNumber(int indexNumber) {
		this.indexNumber = indexNumber;
	}
}
