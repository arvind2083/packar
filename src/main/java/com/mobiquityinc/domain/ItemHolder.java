package com.mobiquityinc.domain;

import java.math.BigDecimal;
import java.util.List;

/**
 * This class will hold the Package related properties of the each line.
 * 
 * @author Arvind
 *
 */
public class ItemHolder {

	/**
	 * This represents the maximum allowed wait for each package.
	 */
	private BigDecimal maxWeight;

	/**
	 * This represents the sorted in the descending order of cost per line in
	 * the input file.
	 */
	private List<Item> sortedList;

	/**
	 * This represented original list of items (before sorting)
	 */
	private List<Item> originalList;

	/**
	 * @return the maxWeight
	 */
	public BigDecimal getMaxWeight() {
		return maxWeight;
	}

	/**
	 * @return the sortedList
	 */
	public List<Item> getSortedList() {
		return sortedList;
	}

	/**
	 * @param sortedList
	 *            the sortedList to set
	 */
	public void setSortedList(List<Item> sortedList) {
		this.sortedList = sortedList;
	}

	/**
	 * @return the originalList
	 */
	public List<Item> getOriginalList() {
		return originalList;
	}

	/**
	 * @param originalList
	 *            the originalList to set
	 */
	public void setOriginalList(List<Item> originalList) {
		this.originalList = originalList;
	}

	/**
	 * @param maxWeight
	 *            the maxWeight to set
	 */
	public void setMaxWeight(BigDecimal maxWeight) {
		this.maxWeight = maxWeight;
	}
}
