package com.mobiquityinc.serviceImpl;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.mobiquityinc.domain.Item;
import com.mobiquityinc.domain.ItemHolder;
import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.service.FileReader;
import com.mobiquityinc.util.Constants;

/**
 * This class will parse the file.
 * 
 * @author Arvind
 *
 */
public class FileReaderImpl implements FileReader {

	private static final Logger LOGGER = Logger.getLogger(FileReaderImpl.class.getName());

	/**
	 * This method will process process the input file and populate the
	 * {@link ItemHolder} object per line of the file.
	 * <p>
	 * Also, the validation rules will checked in the method.
	 */
	public List<ItemHolder> processFile(String filePath) {
		Charset charset = Charset.forName("ISO_8859_1");
		try {
			return Files.readAllLines(Paths.get(filePath), charset).stream().map(this::readLineByLine)
					/** Validation for maximum weight for a package */
					.filter(itemHolder -> itemHolder.getMaxWeight().compareTo(Constants.MAX_PACKAGE_WEIGHT) <= 0)
					/** Validation if number of items more than 15. */
					.filter(itemHolder -> itemHolder.getSortedList().size() <= Constants.MAX_ITEMS_PER_PACKAGE)
					.collect(Collectors.toList());
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			throw new APIException(e.getMessage());
		}
	}

	/**
	 * This method will take the one line in the file at a time and return the
	 * Object representation of the line.
	 * 
	 * @param line
	 * @return ItemHolder
	 */
	private ItemHolder readLineByLine(String line) {
		ItemHolder itemHolder = new ItemHolder();
		List<Item> originalItemList = new ArrayList<>();
		/** Split the file based upon the Colon */
		String[] breakLine = line.split(Constants.COLON);
		/** Get the maximum weight for the line */
		int maxWeight = Integer.parseInt(breakLine[0].replaceAll(Constants.REMOVE_BLANK_SPACE, Constants.EMPTYSTRING));
		Pattern itemPattern = Pattern.compile(Constants.PATTERN);
		Matcher itemMatcher = itemPattern.matcher(breakLine[1]);
		while (itemMatcher.find()) {
			Item item = new Item();
			/**
			 * filter out the item in case the weight is greater than allowed
			 */
			item.setIndexNumber(Integer.parseInt(itemMatcher.group(1)));
			item.setWeight(new BigDecimal(itemMatcher.group(2)));
			item.setCost(new BigDecimal(itemMatcher.group(3)));
			/**Skip the records in case then are more than allowed limit of weight*/
			if(item.getWeight().compareTo(new BigDecimal(maxWeight)) > 0){
				continue;
			}
			/** Validation, skip the records in case the cost is greater 100 */
			if (item.getCost().compareTo(Constants.MAX_ITEM_COST) > 0) {
				LOGGER.log(Level.INFO,
						"Cost of a Item is greater than more than allowed " + 100 + " skipping the item");
				continue;
				/**
				 * Validation, skip the records in case the weight is greater
				 * 100
				 */
			} else if (item.getWeight().compareTo(Constants.MAX_ITEM_WEIGHT) > 0) {
				LOGGER.log(Level.INFO,
						"Weight of a Item is greater than more than allowed " + 100 + " skipping the item");
				continue;
			} else {
				originalItemList.add(item);
			}

		}
		/** Sort the list based upon the cost descending Order */
		List<Item> sortedItemList = originalItemList.stream()
				.sorted((item1, item2) -> item2.getCost().compareTo(item1.getCost())).collect(Collectors.toList());

		/** Populate the ItemHolder */
		itemHolder.setSortedList(sortedItemList);
		itemHolder.setMaxWeight(new BigDecimal(maxWeight));
		itemHolder.setOriginalList(originalItemList);
		return itemHolder;
	}

}
