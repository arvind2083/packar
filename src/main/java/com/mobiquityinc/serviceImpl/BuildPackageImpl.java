package com.mobiquityinc.serviceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.mobiquityinc.domain.Item;
import com.mobiquityinc.domain.ItemHolder;
import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.service.BuildPackage;

/**
 * This is the implementation class for {@link BuildPackage}.
 * 
 * @author Arvind
 *
 */
public class BuildPackageImpl implements BuildPackage {

	private static final Logger LOGGER = Logger.getLogger(BuildPackageImpl.class.getName());

	/**
	 * This is the final method which will create the package based the the
	 * specification.
	 */
	public String buildPackage(List<ItemHolder> itemList) {
		try {
			return String.join("\n",
					itemList.stream().map(this::buildPackge).map(this::listToString).collect(Collectors.toList()));
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			throw new APIException(ex.getMessage());
		}

	}
     
	/**
	 * This method will convert each element in a list to string 
	 * separated by the ",". In case list is empty, then "-" will
	 * be return.
	 * 
	 * @param itemList
	 * @return
	 */
	private String listToString(List<Integer> itemList) {
		return itemList.isEmpty() ? "-"
				: String.join(",", itemList.stream().map(i -> String.valueOf(i)).collect(Collectors.toList()));

	}

	/**
	 * This method will accept the {@link ItemHolder} as input and print the
	 * index numbers of the {@link Item} on the console.
	 * 
	 * @param itemHolder
	 * @return
	 */
	public List<Integer> buildPackge(ItemHolder itemHolder) {
		BigDecimal currentItemWeight = new BigDecimal(0);
		BigDecimal weight = new BigDecimal(0);
		BigDecimal preCost = new BigDecimal(0);

		/**
		 * This variable is defined to check if atleast one item matched the
		 * criteria. Otherwise print the "-" as per the requirement.
		 */
		boolean ifRecordExist = false;
		List<Item> sortedList = itemHolder.getSortedList();

		/**
		 * This list will contain all the index number of Items fulfulling the
		 * criteria
		 */
		List<Integer> finalList = new ArrayList<>();
		for (int cnt = 0; cnt < sortedList.size(); cnt++) {
			currentItemWeight = currentItemWeight.add(sortedList.get(cnt).getWeight());
			BigDecimal currentItemCost = sortedList.get(cnt).getCost();

			/**
			 * Below logic is only applicable if there are Items with equal cost
			 * in the Itemholder.They will always be available consecutively
			 * because the List is sorted in the descending order of cost.
			 * 
			 * If cost of previous record is same as current and weight of
			 * previous record is less then flow will go in the loop.
			 * 
			 * It will substract the previous Item weight from the
			 * currentItemWeight and also remove the previous index only if the
			 * sum of the weight for the current record and previous record is
			 * more than allowed.
			 */
			if (cnt > 0) {
				BigDecimal previousItemWeight = sortedList.get(cnt - 1).getWeight();
				BigDecimal curItemWeight = sortedList.get(cnt).getWeight();
				if (currentItemCost.compareTo(preCost) == 0 && curItemWeight.compareTo(previousItemWeight) <= 0
						&& curItemWeight.add(previousItemWeight).compareTo(itemHolder.getMaxWeight()) >= 0) {
					/** Remove the record added in the previous cycle */
					finalList.remove(finalList.size() - 1);
					/** Substract the weight added in the previous cycle */
					currentItemWeight = currentItemWeight.subtract(sortedList.get(cnt - 1).getWeight());
				}
			}
			/**
			 * This logic will keep adding the index if the sum of
			 * items(currentItemWeight) is less than the allowed maximum weight.
			 */
			if (itemHolder.getMaxWeight().compareTo(currentItemWeight) >= 0) {
				weight = weight.add(currentItemWeight);
				/** Index will be saved in the list */
				finalList.add(sortedList.get(cnt).getIndexNumber());
				ifRecordExist = true;
			} else {
				/**
				 * reset the current Item weight by adding the weight of current
				 * loop
				 */
				currentItemWeight = weight;
			}
			/** reset the current cost by adding the cost of current loop */
			preCost = currentItemCost;
		}
		return finalList;
		/** If there is no record fullfilling the criteria, print the "-" */
		/*
		 * if (!ifRecordExist) { return "-"; } else {
		 *//** convert the finalList into String List and print values *//*
																		 * return
																		 * String
																		 * .join
																		 * (",",
																		 * finalList
																		 * .
																		 * stream
																		 * ().
																		 * map(
																		 * val
																		 * ->
																		 * String
																		 * .
																		 * valueOf
																		 * (val)
																		 * ).
																		 * collect
																		 * (
																		 * Collectors
																		 * .
																		 * toList
																		 * ()));
																		 * }
																		 */
	}
}
