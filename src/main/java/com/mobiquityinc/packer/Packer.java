package com.mobiquityinc.packer;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mobiquityinc.domain.ItemHolder;
import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.service.BuildPackage;
import com.mobiquityinc.service.FileReader;
import com.mobiquityinc.serviceImpl.BuildPackageImpl;
import com.mobiquityinc.serviceImpl.FileReaderImpl;

/**
 * This is initiation class for the program.
 * 
 * @author Arvind
 */
public class Packer {

	/**
	 * Define the logger.
	 */
	private static final Logger LOGGER = Logger.getLogger(Packer.class.getName());

	/**
	 * This the entry point for the program.The main method accepts the file
	 * path as parameter.
	 * <p>
	 * Array of parameters are expected as input in the method.First parameter
	 * must be path of the file. In case more than one parameter is passed in
	 * the input array, other parameters will be ignored. If non of the
	 * parameter is passed in the input array, it will throw an
	 * {@link APIException}. Also, in case program encountered some program
	 * during execution, then also {@link APIException} will be throw.
	 * 
	 * @param args
	 */
	public static void main(final String[] args) throws APIException {
		/**
		 * Minimum one that is file path must be passed as commandline parameter
		 */
		if (args.length == 1) {
			try {
				System.out.println(pack(args[0]));
			} catch (APIException e) {
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
			}
		} else {
			LOGGER.log(Level.SEVERE, "Please pass the filepath with commandLine as first arguement");
			throw new APIException("Please pass the filepath with commandLine as first arguement");
		}
	}

	public static String pack(String filePath) {
		final FileReader processFile = new FileReaderImpl();
		final BuildPackage buildPackageImpl = new BuildPackageImpl();
		/**
		 * This call will parse the file and return the each line of file into
		 * {@link ItemHolder}.
		 */
		List<ItemHolder> itemHolderList = processFile.processFile(filePath);
		/**
		 * This call will finally build the Package as per the specifications
		 * and print the value on the console.
		 */
		return buildPackageImpl.buildPackage(itemHolderList);

	}
}