package com.mobiquityinc.exception;

/**
 * This is Application exception implementation in case any error during
 * execution of the program.
 * 
 * @author Arvind
 */
public class APIException extends RuntimeException {

	/**
	 * serialVersionUID of the class.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 *            message of the error.
	 */
	public APIException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 *            cause of the error.
	 */
	public APIException(Exception cause) {
		super(cause);
	}
}
