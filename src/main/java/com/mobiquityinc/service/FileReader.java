package com.mobiquityinc.service;

import java.util.List;

import com.mobiquityinc.domain.ItemHolder;

/**
 * This Interface methods will process the input file.
 * 
 * @author Arvind
 *
 */
public interface FileReader {
	/**
	 * This method will parse the each line of the file and populate the
	 * {@link ItemHolder} Object.
	 * 
	 * @param filePath
	 * @return return the List of {@link ItemHolder}
	 */
	public List<ItemHolder> processFile(String filePath);
}
