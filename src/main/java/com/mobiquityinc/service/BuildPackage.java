package com.mobiquityinc.service;

import java.util.List;

import com.mobiquityinc.domain.ItemHolder;

/**
 * This interface method will create the packages based upon the specifications
 * for the package and print on the console.
 * 
 * @author Arvind
 *
 */
public interface BuildPackage {
	/**
	 * This method will accept a list of {@link ItemHolder} and print the
	 * outcome on the console.
	 * 
	 * @param itemList
	 * 				 {@link ItemHolder} accept the input List
	 * @return
	 */
	public String buildPackage(List<ItemHolder> itemList);
}
