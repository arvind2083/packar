/**
 * 
 */
package com.mobiquityinc.util;

import java.math.BigDecimal;

/**
 * This file contains the application constants.
 * @author Arvind
 *
 */
public class Constants {
	public static final String REMOVE_BLANK_SPACE = "\\s";
	public static final String COLON = ":";
	public static final String EMPTYSTRING = "";
	public static final String PATTERN = "\\((\\d+),(\\d+(?:\\.\\d{1,2})?),\\D*(\\d+(?:\\.\\d{1,2})?)\\)";

	/** Validation specification constant */
	public static final int MAX_ITEMS_PER_PACKAGE = 15;
	public static final BigDecimal MAX_PACKAGE_WEIGHT = BigDecimal.valueOf(100);
	public static final BigDecimal MAX_ITEM_WEIGHT = BigDecimal.valueOf(100);
	public static final BigDecimal MAX_ITEM_COST = BigDecimal.valueOf(100);

}
