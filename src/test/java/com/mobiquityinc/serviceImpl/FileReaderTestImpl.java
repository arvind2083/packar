package com.mobiquityinc.serviceImpl;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.mobiquityinc.domain.Item;
import com.mobiquityinc.domain.ItemHolder;
import com.mobiquityinc.mock.ItemHolderMock;

/**
 * Test cases for validating the {@link FileReaderImpl} class.
 *
 * @see FileReaderImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class FileReaderTestImpl {

	@InjectMocks
	private FileReaderImpl fileReaderImpl = new FileReaderImpl();

	@Test
	public void processValidFile() {
		List<ItemHolder> validItemHolder = fileReaderImpl.processFile(ItemHolderMock.VALID_INPUT);

		/** Create test data for valid input */
		List<ItemHolder> itemHolderTest = new ArrayList<>();
		ItemHolder itemHolderTest1 = new ItemHolder();
		itemHolderTest1.setMaxWeight(new BigDecimal(81));

		List<Item> itemList = new ArrayList<>();

		Item item1 = new Item();
		item1.setCost(new BigDecimal(100));
		item1.setIndexNumber(1);
		item1.setWeight(new BigDecimal(53.38));
		itemList.add(item1);

		Item item2 = new Item();
		item2.setCost(new BigDecimal(98));
		item2.setIndexNumber(2);
		item2.setWeight(new BigDecimal(88.62));
		itemList.add(item2);

		itemHolderTest1.setOriginalList(itemList);

		ItemHolder itemHolderTest2 = new ItemHolder();
		itemHolderTest2.setMaxWeight(new BigDecimal(20));

		Item item3 = new Item();
		item3.setCost(new BigDecimal(34));
		item3.setIndexNumber(1);
		item3.setWeight(new BigDecimal(15.3));
		itemList.add(item3);

		itemHolderTest.add(itemHolderTest1);
		itemHolderTest.add(itemHolderTest2);

		assertTrue(81 == validItemHolder.get(0).getMaxWeight().intValue());
		for (Item item : validItemHolder.get(0).getOriginalList()) {
			assertTrue(item1.getIndexNumber() == item.getIndexNumber());
			assertTrue(item1.getCost().intValue() == item.getCost().intValue());
			assertTrue(item1.getWeight().intValue() == (item.getWeight()).intValue());
		}

		for (Item item : validItemHolder.get(1).getOriginalList()) {
			assertTrue(item3.getIndexNumber() == item.getIndexNumber());
			assertTrue(item3.getCost().intValue() == item.getCost().intValue());
			assertTrue(item3.getWeight().intValue() == (item.getWeight()).intValue());
		}

	}

	@Test
	public void processInValidFile() {
		List<ItemHolder> inValidItemHolder = fileReaderImpl.processFile(ItemHolderMock.INVALID_INPUT);

		/** Create test data for valid input */
		ItemHolder itemHolderTest1 = new ItemHolder();
		itemHolderTest1.setMaxWeight(new BigDecimal(8));

		List<Item> itemList = new ArrayList<>();

		Item item1 = new Item();
		item1.setCost(new BigDecimal(34));
		item1.setIndexNumber(1);
		item1.setWeight(new BigDecimal(15.3));
		itemList.add(item1);

		assertTrue(8 == inValidItemHolder.get(0).getMaxWeight().intValue());
		for (Item item : inValidItemHolder.get(0).getOriginalList()) {
			assertTrue(item1.getIndexNumber() != item.getIndexNumber());
			assertTrue(item1.getCost().intValue() != item.getCost().intValue());
			assertTrue(item1.getWeight().intValue() != (item.getWeight()).intValue());
		}

	}

}
