package com.mobiquityinc.serviceImpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.mobiquityinc.domain.Item;
import com.mobiquityinc.domain.ItemHolder;

@RunWith(MockitoJUnitRunner.class)
public class BuildPackageTestImpl {

	private BuildPackageImpl buildPackageImpl = new BuildPackageImpl();

	/**
	 * Test scenario : When the maximum weight is less in all the items present
	 * in the ItemHolder.
	 */
	@Test
	public void testMaxWeightLessThanRecords() {

		ItemHolder itemHolderTest = new ItemHolder();
		itemHolderTest.setMaxWeight(new BigDecimal(10));

		List<Item> itemList = new ArrayList<>();
		Item item = new Item();
		item.setCost(new BigDecimal(34));
		item.setIndexNumber(1);
		item.setWeight(new BigDecimal(15.3));
		itemList.add(item);

		itemHolderTest.setOriginalList(itemList);
		itemHolderTest.setSortedList(itemList);
		List<ItemHolder> itemHolderList = new ArrayList<>();
		itemHolderList.add(itemHolderTest);

		String outcome = buildPackageImpl.buildPackage(itemHolderList);
		assertEquals("-", outcome);
		assertEquals(1, outcome.length());
		assertTrue("Test case with no matching criteria", true);
	}

	/**
	 * Test scenerio : When 2 records having same cost but different weight
	 * under allowed maximum weight limit
	 */
	@Test
	public void testSameCostDifferentWeight() {

		ItemHolder itemHolderTest = new ItemHolder();
		itemHolderTest.setMaxWeight(new BigDecimal(50));

		List<Item> itemList = new ArrayList<>();
		Item item = new Item();
		item.setCost(new BigDecimal(34));
		item.setIndexNumber(1);
		item.setWeight(new BigDecimal(15.3));

		Item item1 = new Item();
		item1.setCost(new BigDecimal(34));
		item1.setIndexNumber(2);
		item1.setWeight(new BigDecimal(15.1));

		itemList.add(item);
		itemList.add(item1);

		itemHolderTest.setOriginalList(itemList);
		itemHolderTest.setSortedList(itemList);

		List<ItemHolder> itemHolderList = new ArrayList<>();
		itemHolderList.add(itemHolderTest);

		String outcome = buildPackageImpl.buildPackage(itemHolderList);
		String[] str = outcome.split(",");
		assertTrue("testSameCostDifferentWeight Passed", 1 == Integer.valueOf(str[0]) && 2 == Integer.valueOf(str[1]));
	}

	/**
	 * Test scenerio : When 3 records having same cost but different weight
	 */

	@Test
	public void test3RecordsSameCostDifferentWeight() {

		ItemHolder itemHolderTest = new ItemHolder();
		itemHolderTest.setMaxWeight(new BigDecimal(50));

		List<Item> itemList = new ArrayList<>();
		Item item = new Item();
		item.setCost(new BigDecimal(34));
		item.setIndexNumber(1);
		item.setWeight(new BigDecimal(15.3));

		Item item1 = new Item();
		item1.setCost(new BigDecimal(33));
		item1.setIndexNumber(2);
		item1.setWeight(new BigDecimal(15.1));

		Item item2 = new Item();
		item2.setCost(new BigDecimal(32));
		item2.setIndexNumber(3);
		item2.setWeight(new BigDecimal(15.0));

		itemList.add(item);
		itemList.add(item1);
		itemList.add(item2);

		itemHolderTest.setOriginalList(itemList);
		itemHolderTest.setSortedList(itemList);
		
		List<ItemHolder> itemHolderList = new ArrayList<>();
		itemHolderList.add(itemHolderTest);

		String outcome = buildPackageImpl.buildPackage(itemHolderList);
		String[] str = outcome.split(",");
		assertTrue("testSameCostDifferentWeightMoreThanAllowedWeight Passed",
				1 == Integer.valueOf(str[0]) && 2 == Integer.valueOf(str[1]) && 3 == Integer.valueOf(str[2]));

	}

}