package com.mobiquityinc.main;

import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.mobiquityinc.mock.ItemHolderMock;
import com.mobiquityinc.packer.Packer;


/**
 * @author Arvind
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class Packar {

	/**
	 * Tests if the application handles all its exceptions internally.
	 *
	 * @see Packer#main(String[])
	 */

	@Test
	public void testMain() {
		try {
			Packer.main(new String[] {});
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
    /**
     * Tests if the application handles all its exceptions internally.
     *
     * @see Packer#main(String[])
     */
    @Test
    public void testMainValidInput() {
        try {
        	Packer.main(new String[]{ItemHolderMock.VALID_INPUT});
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Tests if the application handles all its exceptions internally.
     *
     * @see Packer#main(String[])
     */
    @Test
    public void testMainInvalidInput() {
        try {
        	Packer.main(new String[]{ItemHolderMock.INVALID_INPUT});
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
