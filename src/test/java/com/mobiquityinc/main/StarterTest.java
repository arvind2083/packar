package com.mobiquityinc.main;

import static org.junit.Assert.fail;
import java.util.jar.Pack200.Packer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.mobiquityinc.mock.ItemHolderMock;

/**
 * @author Arvind
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class StarterTest {

	/**
	 * Tests if the application handles all its exceptions internally.
	 *
	 * @see Starter#main(String[])
	 */

	@Test
	public void testMain() {
		try {
			Starter.main(new String[] {});
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
    /**
     * Tests if the application handles all its exceptions internally.
     *
     * @see Packer#main(String[])
     */
    @Test
    public void testMainValidInput() {
        try {
        	Starter.main(new String[]{ItemHolderMock.VALID_INPUT});
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Tests if the application handles all its exceptions internally.
     *
     * @see Packer#main(String[])
     */
    @Test
    public void testMainInvalidInput() {
        try {
        	Starter.main(new String[]{ItemHolderMock.INVALID_INPUT});
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
