package com.mobiquityinc.mock;

public class ItemHolderMock {
    /**
     * The path for a valid input file
     */
    public static final String VALID_INPUT = "src//test/resources//validFile.txt";
    /**
     * The path for an invalid input file
     */
    public static final String INVALID_INPUT = "src//test//resources//invalidFile.txt";
}
